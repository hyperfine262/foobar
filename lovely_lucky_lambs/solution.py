def Pows(n):
    remaining = n
    current = 1
    count = 0
    while remaining >= 0:
        count += 1
        remaining -= current
        current *= 2
    current /= 2
    remaining += current
    twoJuniorsSum = (current/4) + (current/2)
    if twoJuniorsSum <= remaining:
        count += 1
    return count-1

def Fibs(n):
    remaining = n
    low = 0
    high = 1
    count = 0
    while remaining > 0:
        count += 1
        temp = low + high
        remaining -= temp
        low = high
        high = temp
    return count

def answer(n):
    if n < 10 or n > 10**9:
        return 0
    return Fibs(n) - Pows(n)
