def answer(l):
    prevDivisors = [0 for i in range(len(l))]
    tripleCount = 0
    for k in range(0, len(l)):
        for j in range(0, k):
            if l[k] % l[j] is 0:
                prevDivisors[k] += 1
                tripleCount += prevDivisors
    return tripleCount
