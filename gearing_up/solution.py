from __future__ import division
import copy
from fractions import Fraction

def rReduce(x, y, pivot):
    x_coeff = x[pivot]
    y_coeff = y[pivot]
    for i in range(len(x)):
        y[i] = y_coeff*x[i] - x_coeff*y[i]
    return 

def buildB(pegs):
    result =[]
    for i in range(len(pegs)):
        temp = pegs[-1] - pegs[i]
        result.append(temp)
    return result

def answer(pegs):
    b = buildB(pegs)
    n= len(b)
    
    matrix = []
    for i in range(n):
        row = [0]*n
        for j in range(i, n):
            if (j==i):
                row[j]=1
                continue
            if (j==n-1):
                row[j]=1
                break
            else:
                row[j]=2
        matrix.append(row)
    
    matrix[-1][0] = 1
    matrix[-1][-1] = -2
    
    for i in range(len(b)):
        matrix[i].append(b[i])
    
    i = 0
    for row in matrix[:-1]:
        rReduce(row, matrix[-1],i)
        i += 1
    
    i = 1
    j = 0
    for i in range(1,n):
        for j in range(i):
            rReduce(matrix[i],matrix[j],i)
    
    gears = []
    int_gears = []
    for i in range(len(matrix)):
            try:
                gears.append(matrix[i][-1]/matrix[i][i])
                int_gears.append([matrix[i][-1],matrix[i][i]])
            except:
                return [-1,-1]
    
    for gear in gears:
        if (gear >= 1):
            continue
        else:
            return [-1,-1]
    
    result = int_gears[0]
    return [Fraction(result[0],result[1]).numerator, Fraction(result[0],result[1]).denominator ]
    
#pegs = [4,30,50]
#print answer(pegs)
#pegs = [4,17,30,44]
#print answer(pegs)
#pegs = [2,12]
#print answer(pegs)
