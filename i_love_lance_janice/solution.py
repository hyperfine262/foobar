def answer(str):
    key = 'abcdefghijklmnopqrstuvwxyz'
    result = ''
    for x in str:
        try:
            i = 25 - key.index(x)
            result += key[i]
        except:
            result += x
    return result

