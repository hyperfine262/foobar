from fractions import gcd
from fractions import Fraction as F

def lcm(a,b):
    return (a*b)/gcd(a,b)

def prettify(nums,denoms):
    least_common = 1
    factors = []
    for denom in denoms:
        least_common = lcm(least_common, denom)
        factors.append(least_common/denom)
    for i in range(len(factors)):
        nums[i] = nums[i]*factors[i]
        denoms[i] = denoms[i]*factors[i]    
    least_common = lcm(least_common,denoms[0])
    factor = least_common/denoms[0]
    nums[0] *= factor
    denoms[0] *= factor
    return

def testDenoms(denoms):
    test = denoms[0]
    for denom in denoms:
        if test != denom:
            return False
    return True

def setCommonDenom(nums,denoms):
    test = False
    while test==False:
        prettify(nums,denoms)
        test = testDenoms(denoms)
    
def swapPositions(row,i,j):
    temp = row[j]
    row[j] = row[i]
    row[i] = temp

def swapRows(A,i,j):
    for row in A:
        swapPositions(row,i,j)
    temp = A[j]
    A[j] = A[i]
    A[i] = temp

def isZero(row):
    for entry in row:
        if entry != 0:
            return False
    return True

def swapIfNeeded(A,i,j):
    bottom_zero = isZero(A[i])
    top_zero = isZero(A[j])
    if (top_zero and not bottom_zero):
        swapRows(A,i,j)
        return True
    return False

def order(A):
    n = len(A)
    test = 1
    while(test > 0):
        test = 0
        for i in range(1,n):
            if (swapIfNeeded(A, n-i, n-(i+1))):
                test += 1
                break
            else:
                continue
    return

def transposeMatrix(m):
    t = []
    for r in range(len(m)):
        tRow = []
        for c in range(len(m[r])):
            if c == r:
                tRow.append(m[r][c])
            else:
                tRow.append(m[c][r])
        t.append(tRow)
    return t

def getMatrixMinor(m,i,j):
    return [row[:j] + row[j+1:] for row in (m[:i]+m[i+1:])]

def getDeterminant(m):
    if len(m) == 2:
        return m[0][0]*m[1][1]-m[0][1]*m[1][0]
    determinant = 0
    for c in range(len(m)):
        determinant += ((-1)**c)*m[0][c]*getDeterminant(getMatrixMinor(m,0,c))
    return determinant

def invertMatrix(m):
    determinant = getDeterminant(m)
    if len(m) == 2:
        return [[m[1][1]/determinant, -1*m[0][1]/determinant],
                [-1*m[1][0]/determinant, m[0][0]/determinant]]
    cofactors = []
    for r in range(len(m)):
        cofactorRow = []
        for c in range(len(m)):
            minor = getMatrixMinor(m,r,c)
            cofactorRow.append(((-1)**(r+c)) * getDeterminant(minor))
        cofactors.append(cofactorRow)
    cofactors = transposeMatrix(cofactors)
    for r in range(len(cofactors)):
        for c in range(len(cofactors)):
            cofactors[r][c] = cofactors[r][c]/determinant
    return cofactors

def normalizeM(m):
    P = []
    for row in m:
        new_row = []
        sum = 0
        for entry in row:
            sum += entry
        if (sum > 0):
            for entry in row:
                new_row.append(F(entry, sum))
        else:
            for entry in row:
                new_row.append(F(0,1))
        P.append(new_row)
    return P

def findTerminalStates(m):
    result = []
    index = 0
    for row in m:
        sum = 0
        for entry in row:
            sum += entry
        if (sum == 0):
            result.append(index)
        index += 1
    return result

def buildQ(P,terminal_states):
    n = len(P) - len(terminal_states)
    Q = []
    for i in range(n):
        row = []
        for j in range(n):
            row.append(P[i][j])
        Q.append(row)
    return Q

def buildI(n):
    I = []
    for i in range(n):
        row = []
        for j in range(n):
            if (i==j):
                row.append(F(1,1))
            else:
                row.append(F(0,1))
        I.append(row)
    return I
    print row

def buildN(Q):
    n = len(Q)
    N = buildI(n)
    for i in range(n):
        for j in range(n):
            N[i][j] -= Q[i][j]
    return N

def buildR(P, terminal_states):
    m = len(P) - len(terminal_states)
    n = len(terminal_states)
    R = []
    for i in range(m):
        row = []
        for j in range(n):
            entry = P[i][m+j]
            row.append(entry)
        R.append(row)
    return R

def fracMatrixProduct(A, B):
    rows_A = len(A)
    cols_A = len(A[0])
    rows_B = len(B)
    cols_B = len(B[0])

    C = [[F(0,1) for row in range(cols_B)] for col in range(rows_A)]

    for i in range(rows_A):
        for j in range(cols_B):
            for k in range(cols_A):
                C[i][j] += A[i][k] * B[k][j]
    return C

def answer(m):
    if (len(m)==1):
        return [1,1]
    order(m)
    P = normalizeM(m)
    terminal_states = findTerminalStates(P)
    R =  buildR(P, terminal_states)
    Q = buildQ(P, terminal_states)
    N_inv = buildN(Q)
    N = invertMatrix(N_inv)
    result = fracMatrixProduct(N,R)
    
    nums = []
    denoms = []
    for num in result[0]:
        nums.append(num.numerator)
        denoms.append(num.denominator)

    setCommonDenom(nums,denoms)
    nums.append(denoms[0])
    return nums

##############
### Main ####
##############

M = [
     [0,1,0,0,0,1],
     [4,0,0,3,2,0],
     [0,0,0,0,0,0],
     [0,0,0,0,0,0],
     [0,0,0,0,0,0],
     [0,0,0,0,0,0]
    ]

#m = [[0, 2, 1, 0, 0], [0, 0, 0, 3, 4], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
n = [0]

m9 = [[0, 86, 61, 189, 0, 18, 12, 33, 66, 39], [0, 0, 2, 0, 0, 1, 0, 0, 0, 0], [15, 187, 0, 0, 18, 23, 0, 0, 0, 0], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
a9 = [6, 44, 4, 11, 22, 13, 100]

m8 = [[1, 1, 1, 0, 1, 0, 1, 0, 1, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [1, 0, 1, 0, 1, 1, 1, 0, 1, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [1, 0, 1, 0, 1, 0, 1, 1, 1, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [1, 0, 1, 0, 1, 0, 1, 0, 1, 1],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

#a8 = [2, 1, 1, 1, 1, 6]


print answer(m8)
#print answer(M)
#print answer(n)

