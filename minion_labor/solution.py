def answer(data, n):
    if length(data) == 0:
        return []
    if n==0:
        #print "[]" 
        return [] 
    counts = []
    counts.append([data[0], 0 ])
    bad_nums = []

    for datum in data:
        found = False
        for count in counts:
            if datum == count[0]:
                count[1] += 1
                found = True
                continue

        if not found:
            counts.append([datum, 1])

    for count in counts:
        if count[1] > n:
            bad_nums.append(count[0])

    result = []
    for datum in data:
        passed = True
        for num in bad_nums:
            if num == datum:
                passed = False
                break
        if passed == True:
            result.append(datum)

    #print result
    return result

#data = [1,2,4,4,5,4,5,3]
#answer(data, 0)
#answer(data, 1)
#answer(data, 2)
#answer(data, 3)

data1 = [1,2,3]
n1 = 0

answer(data1, n1)

data2 = [1,2,2,3,3,3,4,5,5]
n2 = 1

answer(data2, n2)

data3 = [1,2,3]
n3 = 6

answer(data3, n3)

