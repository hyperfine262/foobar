def answer(n):
    table = [[0 for i in range(n+1)] for j in range(n+1)]
    table[0][0] = 1

    for last in range(1, n+1):
        for remaining in range(0, n+1):
            table[last][remaining] = table[last - 1][remaining]
            if remaining >= last:
                table[last][remaining] += table[last - 1][remaining - last]
    return (table[n][n] - 1)

for i in range(3,20):
    print answer(i)



